const menuBtn = document.querySelector(".menu-btn");
const menu = document.querySelector(".clickable-menu");

menuBtn.addEventListener("click", () => {
  menuBtn.classList.toggle("active");
  menu.classList.toggle("active");
});
